# Javascript Jest for unitTest

### How to install Jest and use it
Need to run these into command line:

`npm install --save-dev Jest`

`npm install --save-dev babel-jest @babel/core @babel/preset-env`

Then you need to create a file as the name `.babelrc` and set the next content

    {
      "presets": ["@babel/preset-env"]
    }

On the file `package.json` needs to set the next information:

    "devDependencies": {
        "@babel/core": "^7.4.4",
        "@babel/preset-env": "^7.4.4",
        "babel-jest": "^24.8.0",
        "jest": "^24.8.0"
    }

Also on the scripts sections, if you want

    "scripts": {
        "test": "jest"
    },

And then you can run it like this: `npm test`

### Start coding
Create a folder with the name `__test__` , then jest will recognize you are going to save all your tests file inside this folder.

Creating a file should have the following name: `nameFile.test.js`

### Commands line
Runs only an specific file:
`npm test nameFile.test.js`

### Best practices
Always creates a `describe` for each file test even if there would be only one test.

### Snapshots
Command to update snapshopts:
`npm test -- -u`

### Extra things
- If you want to create mock servers to test your UI: [Mocky](https://designer.mocky.io/ "Mocky")

-------------

- We use `axios` instead of `fetch` or `XMLHttpRequest` because with `axios` can runs on every browser even if it is old, with `fetch` we have errors if the browser cannot support it.

-------------

This URL can allow you to return any response you need [Mock status](http://httpstat.us "Mock status")

for example to return an error of 404: [Mock status 404](http://httpstat.us/404 "Mock status 404")