//Este archivo sirve para probar cosas sin tener que importar modulos para probarlos, aqui todo se hace

describe('Comparadores comunes', () => {
  const user = {
    name: 'Carlos',
    lastName: 'Peres'
  }

  const user2 = {
    name: 'Carlos',
    lastName: 'Peres'
  }

  test('igualdad de elementos', () => {
    expect(user).toEqual(user2);
  });

  test('No son exactamente iguales', () => {
    expect(user).not.toEqual(user2)
  });
});