import { sumar, multiplicar, restar, dividir } from '../maths.js';

//El describe es un Test Suite
describe('Calculos matematicos', () => {
  //Estos test son los Tests que se muestran en consola
  test('Prueba de sumas', () => {
    expect(sumar(1,1)).toBe(3);
  });

  test('Multiplicar', () => {
    expect(multiplicar(2,2)).toBe(4);
  });

  test('Restar', () => {
    expect(restar(10,5)).toBe(5);
  });

  test('Dividir', () => {
    expect(dividir(20,2)).toBe(10);
  });
});