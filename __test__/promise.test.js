import { getDataFromApi } from '../promise';

describe('Probando promesas', () => {
  test('Realizando una peticion a una API', done => {
    const api = 'https://rickandmortyapi.com/api/character/';
    getDataFromApi(api).then(data => {
      expect(data.results.length).toBeGreaterThan(0);
      done();
    });
  });

  test('Resuelve un Hola!', () => {
    return expect(Promise.resolve('Hola!')).resolves.toBe('Hola!');
  });

  test('Rechaza con un error', () => {
    return expect(Promise.reject('Errror')).rejects.toBe('Errror');
  });
});


//done() = Se le pone esto ya que como es una funcion asyncrona se pone esto para que le avise a jest que ya termino de procesar el api y asi pueda hacer la comprobacion (Esto para que funcione de forma correcta)