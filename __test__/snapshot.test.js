import { getCharacter } from '../snapshot';
import rick from '../rick.json';

describe('Es hora de las instantaneas', () => {
  test('Snapshot', () => {
    expect(getCharacter(rick)).toMatchSnapshot();
  });

  test('Siempre fallara la instancea', () => {
    const user = {
      id: Math.floor(Math.random() * 100),
      createdAt: new Date()
    }
    expect(user).toMatchSnapshot();
  });

  test('Tenemos una exception dentro del codigo', () => {
    const user = {
      id: Math.floor(Math.random() * 100),
      name: "Carlos perez"
    }
    
    expect(user).toMatchSnapshot({
      id: expect.any(Number)
    });
  });
});